/*
** foo.c for Allum1 in /home/asphahyre/rendu/CPE_2015_Allum1/src
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 21 14:32:59 2016 Luka Boulagnon
** Last update Fri Feb 26 17:11:20 2016 Luka Boulagnon
*/

#include "allum1.h"

int		read_line_nb(char *buffer, int *lines)
{
  int		line;

  line = 0;
  while (!line)
    {
      write(1, "Line: ", 6);
      if ((buffer = get_next_line(0)) &&
	  ((line = my_getnbr(buffer)) > NB_LINES || !line))
	write(1, "Error: this line is out of range\n", 33 + (line = 0));
      else if (!buffer)
	break;
      else if (line < 0)
	write(1, "Error: invalid input (positive number expected)\n", 48 + (line = 0));
      else if (!lines[line - 1])
	write(1, "Error: this line is empty\n", 26 + (line = 0));
      free(buffer);
    }
  return (line);
}

void		read_player_move_and_print_updated_board_game()
{
  int		line;
  int		matches;
  int		*lines;
  char		*buffer;

  write(1, "\nYour turn:\n", 12 + (line = 0));
  lines = init(NULL);
  while (!line || print_updated_board_game(line, matches))
    {
      line = read_line_nb(buffer, lines);
      if (line < 1)
	break;
      write(1, "Matches: ", 9);
      buffer = get_next_line(0);
      if (!buffer)
	break;
      matches = my_getnbr(buffer);
      free(buffer);
    }
}

int		lost(int player)
{
  int		*lines;
  int		nb;

  lines = init(NULL);
  nb = NB_LINES;
  while (nb--)
    if (lines[nb])
      return (0);
  if (player == IA)
    write(1, "I lost.. snif.. but I'll get you next time!!\n", 47);
  else
    write(1, "You lost, too bad..\n", 20);
  return (1);
}

void		pick_random_move(int *lines)
{
  int		nb;

  nb = NB_LINES;
  while (nb--)
    if (lines[nb])
      {
	--lines[nb];
	break;
      }
  write(1, "AI removed 1 match(es) from line ", 33);
  my_putnbr(nb + 1);
  write(1, "\n", 1);
  print_board_game_in_a_pretty_frame();
}

int		pick_right_move(int xor, int *lines)
{
  int		nb;

  nb = NB_LINES;
  while (nb--)
    {
      if ((lines[nb] ^ xor) < lines[nb])
	{
	  write(1, "AI removed ", 11);
	  my_putnbr(lines[nb] - (lines[nb] ^ xor));
	  lines[nb] ^= xor;
	  write(1, " match(es) from line ", 21);
	  my_putnbr(nb + 1);
	  write(1, "\n", 1);
	  print_board_game_in_a_pretty_frame();
	  return (1);
	}
    }
  return (0);
}
