/*
** ia.c for  in /home/asphahyre/rendu/CPE_2015_Allum1/src
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 21 14:35:51 2016 Luka Boulagnon
** Last update Fri Feb 26 16:53:14 2016 Luka Boulagnon
*/

#include "allum1.h"

void		ia_turn()
{
  int		*lines;
  int		nb;
  int		xor;

  lines = init(NULL);
  write(1, "\nAI's turn...\n", 14);
  xor = DIFFICULTY + 0 * (nb = NB_LINES);
  while (nb--)
    xor ^= lines[nb];
  if ((nb = NB_LINES) && !xor)
  {
    pick_random_move(lines);
    return ;
  }
  if (xor && pick_right_move(xor, lines))
    return ;
  nb = NB_LINES;
  pick_random_move(lines);
}

int	print_updated_board_game(int line, int matches)
{
  int	*lines;

  if (line > NB_LINES || line < 0)
    return (write(1, "Error: this line is out of range\n", 33));
  lines = init(NULL);
  if (matches == 0)
    return (write(1, "Error: you have to remove at least one match\n", 45));
  if (matches < 0)
    return (write(1, "Error: invalid input (positive number expected)\n", 48));
  if (lines[line - 1] < matches)
    return (write(1, "Error: not enough matches of this line\n", 39));
  lines[line - 1] -= matches;
  write(1, "Player removed ", 15);
  my_putnbr(matches);
  write(1, " match(es) from line ", 21);
  my_putnbr(line);
  write(1, "\n", 1);
  print_board_game_in_a_pretty_frame();
  if (lost(PLAYER))
    return (0);
  ia_turn();
  if (lost(IA))
    return (0);
  read_player_move_and_print_updated_board_game();
  return (0);
}

void	print_board_game_in_a_pretty_frame()
{
  int	cols;
  int	rows;
  int	*lines;

  lines = init(NULL);
  cols = 2 * NB_LINES;
  while (cols--)
    write(1, "*", 1);
  write(1, "*\n", 2);
  rows = 0;
  while (++rows < (NB_LINES + 1))
    {
      cols = -1;
      write(1, "*", 1);
      while (++cols < (2 * NB_LINES - 1))
	write(1, (cols >= (NB_LINES - rows) &&
		  (cols - NB_LINES + rows < lines[rows - 1])) ? "|" : " ", 1);
      write(1, "*\n", 2);
    }
  cols = 2 * NB_LINES;
  while (cols--)
    write(1, "*", 1);
  write(1, "*\n", 2);
}
