/*
** main.c for  in /home/asphahyre/rendu/CPE_2015_Allum1/src
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 21 14:37:45 2016 Luka Boulagnon
** Last update Fri Feb 26 16:59:56 2016 Luka Boulagnon
*/

#include "allum1.h"

int		main()
{
  print_board_game_in_a_pretty_frame();
  read_player_move_and_print_updated_board_game();
  return (0);
}

void		my_putnbr(int nbr)
{
  char		c;

  if (nbr)
    {
      my_putnbr(nbr / 10);
      c = nbr % 10 + 48;
      write(1, &c, 1);
    }
}

int		my_getnbr(char *str)
{
  int		nb;

  if (!*str)
    return (-1);
  nb = 0;
  while (*str)
    {
      if (*str < '0' || *str > '9')
	return (-1);
      nb *= 10;
      nb += *str - '0';
      if (nb > 100000)
	return (100000);
      ++str;
    }
  return (nb);
}

void		my_bzero(char *buff, int size)
{
  while (size--)
    buff[size] = 0;
}

int		*init(int *lines)
{
  static int	done[NB_LINES] = {-1};
  int		nb;

  if (!lines && *done != -1)
    return (done);
  nb = NB_LINES + 1;
  while (*done == -1 && nb-- && lines)
    done[nb] = lines[nb] = nb * 2 + 1;
  while (*done == -1 && nb-- && !lines)
    done[nb] = nb * 2 + 1;
  while (*done != -1 && nb--)
    done[nb] = lines[nb];
  return (done);
}

