##
## Makefile for tekdoom in /gfx_tekdoom
##
## Made by Antoine Baché
## Login   <bache_a@epitech.net>
##
## Started on  Mon Jan 11 20:31:57 2016 Antoine Baché
## Last update Thu Feb 25 15:32:56 2016 Luka Boulagnon
##

SRC_PREFIX=		src/

SRC_FILES=		main.c		\
			core.c		\
			ia.c		\
			get_next_line.c

SRC=			$(addprefix $(SRC_PREFIX),$(SRC_FILES))

NAME=			allum1

NAME_BONUS=		allum1.bonus

CFLAGS=			-Iinclude/ -W -Wall -Wextra -ansi -pedantic

CC=			gcc

RM=			rm -f

OBJ=			$(SRC:.c=.o)

all:	$(NAME)

$(NAME):	$(OBJ)
	@echo -n "[ "
	@echo -n "OK"
	@echo -n " ] "
	@echo "Compiled game"
	@$(CC) $(OBJ) -o $(NAME) $(LIB)

%.o:	%.c
	@echo -n "[ " ; echo -n "OK" ; echo -n " ] "; echo "Compiling" $<
	@$(CC) -o $@ -c $< $(CFLAGS)

clean:
	@echo -n "[ "
	@echo -n "OK"
	@echo -n " ] "
	@echo "Removing OBJ files..."
	@$(RM) $(OBJ)

fclean:	clean
	@echo -n "[ "
	@echo -n "OK"
	@echo -n " ] "
	@echo "Deleting binaries..."
	@$(RM) $(NAME)
	@$(RM) $(NAME_BONUS)

re:	fclean all

bonus:	$(NAME_BONUS)

$(NAME_BONUS):
	ln -s ./bonus/index.js $(NAME_BONUS)

.PHONY:	clean fclean all re debug bonus
