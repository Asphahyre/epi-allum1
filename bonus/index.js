#!/usr/bin/env node

var sdk = require('openbci-sdk'),
    ourBoard = new sdk.OpenBCIBoard({verbose: false}),
    channels = new Array(8),
    derivates = new Array(8),
    lines = new Array(nb_lines),
    get_input = new Array(2),
    what_to_get = 0,
    selected_line,
    selected_matches,
    line_hovered = 1,
    nb_matches = 1,
    bascule = false,
    blink_type,
    blessed = require('blessed'),
    nb_lines = 5,
    mousex = 0,
    mousey = 0,
    difficulty = 5,
    base = 1,
    box,
    logs,
    selected_diff = 0,
    both = 0,
    selecting_matches = false;

/*
** Initializing Blessed...
*/

// Create a screen object.
var screen = blessed.screen({
	smartCSR: true
});

screen.title = 'Allum1';

// Create a box perfectly centered horizontally and vertically.
var diff = blessed.list({
	mouse:			true,
	top:			'center',
	left:			'center',
	width:			'0%+' + (2 + nb_lines * 2),
	height:			'0%+7',
	align:			'center',
	vi:			true,
	keys:			true,
	border:	{
		type:		'line'
	},
	style:	{
		fg:		'red',
		bg:		'black'
	}
});
var get_lines = blessed.list({
	mouse:			true,
	top:			'center',
	left:			'center',
	width:			'0%+10',
	height:			'0%+11',
	align:			'left',
	vi:			true,
	keys:			true,
	border:	{
		type:		'line'
	},
	style:	{
		fg:		'red',
		bg:		'black'
	}
});

// Append our box to the screen.
screen.append(get_lines);
screen.append(diff);

diff.add('Newbie');
diff.add('Easy');
diff.add('Medium');
diff.add('Hard');
diff.add('Impossible');

get_lines.add('4');
get_lines.add('7');
get_lines.add('10');
get_lines.add('15');
get_lines.add('25');
get_lines.add('40');
get_lines.add('60');
get_lines.add('80');
get_lines.add('100');

function getting_difficulty(data) {
	if (data.content.match('Newbie') != null) {
		difficulty = 0;
		base = 0;
	} else if (data.content.match('Easy') != null) {
		difficulty = 10;
		base = 1;
	} else if (data.content.match('Medium') != null) {
		difficulty = 5;
		base = 1;
	} else if (data.content.match('Hard') != null) {
		difficulty = 2;
		base = 1;
	} else {
		difficulty = 0;
		base = 1;
	}
	diff.destroy();
}

diff.on('select', getting_difficulty);

function getting_allumettes(data) {
	if (!diff.destroyed)
		return ;
	if (data.content.match('you') !== null) {
		screen.destroy();
		if (data.content.match('won'))
			console.log('Congratulations... Against all expectation... you... won...');
		else
			console.log('Congratulations, you fool! エネちゃん won against your STUPIDITY!');
		process.exit(0);
	}
	if (data.content.match('|') == null || !lines[data.parent.selected]) {
		logs.setContent('Error: this line is empty');
		screen.render();
		return ;
	}
	box.interactive = false;
	line_hovered = 1 + data.parent.selected;
	logs.setContent('Selected line ' + line_hovered);
	board_game();
	screen.render();
}

function getting_lines(data) {
	nb_lines = parseInt(data.content);
	
	box = blessed.list({
		mouse:			true,
		top:			'center',
		left:			'center',
		width:			'0%+' + (2 + nb_lines * 2),
		height:			'0%+' + ((2 + nb_lines) > screen.height ? (screen.height) : (2 + nb_lines)),
		content:		'',
		tags:			true,
		vi:			true,
		keys:			true,
		transparency:		true,
		border:	{ type:		'line'
		},
		style:	{
			fg:		'white',
			bg:		'black',
			scrollbar: {
				bg:	'red',
				fg:	'blue'
			},
			selected: {
				bold:	true
			}
		},
		selected: {
			style: {
				fg:	'green'
			}
		},
		scrollable:		true,
		scrollbar:		true
	});

	logs = blessed.box({
		transparency:		true,
		bottom:			1,
		left:			10,
		width:			'0%+42',
		height:			'0%+4',
		content:		'First turn...',
		tags:			true,
		border:	{
			type:		'line'
		},
		style:	{
			fg:		'white',
			border:	{
				fg:	'#f0f0f0'
			}
		},
		draggable:		true,
		shadow:			true
	});

	box.on('select', getting_allumettes);

	screen.append(box);
	screen.append(logs);
	get_lines.destroy();
	for (var i = 0 ; i < nb_lines ; ++i)
	    lines[i] = i * 2 + 1;
	board_game();
	screen.render();
}

get_lines.on('select', getting_lines);

function rm_allumettes(data) {
	var matches;

	if (box == undefined || !diff.destroyed)
		return ;
	if (!box.interactive) {
		logs.setContent(logs.content + ', ' + ((matches = (lines[line_hovered - 1] - mousex + nb_lines - line_hovered + 1)) > lines[line_hovered - 1] ?
					(matches = lines[line_hovered - 1]) : matches) + ' matches');
		if (matches <= 0) {
			logs.setContent(logs.content + '\nError: you have to remove at least one match');
			return ;
		}
		lines[line_hovered - 1] -= matches;
		box.interactive = true;
		if (lost(1))
			return ;
		board_game();
		ai_turn();
		if (lost(2))
			return ;
		board_game();
	}
}

screen.on('mousedown', rm_allumettes);

screen.on('mousemove', function(data) {
	if (box == undefined || !diff.destroyed)
		return ;
	mousex = data.x - box.lpos.xi;
	mousey = data.y - box.lpos.yi;
	if (data.x > (box.lpos.xi + 1) && data.x < (box.lpos.xl - 1) &&
	    data.y > (box.lpos.yi + 1) && data.y < (box.lpos.yl - 1)) {
		if (box.interactive) {
			line_hovered = parseInt(box.childBase + mousey);
		}
	}
	board_game();
	box.select(line_hovered - 1);
	screen.render();
});

screen.on('mousemove', function(data) {
	if (box == undefined || !diff.destroyed)
		return ;
	mousex = data.x - box.lpos.xi;
	mousey = data.y - box.lpos.yi;
	if (data.x > box.lpos.xi && data.x < box.lpos.xl &&
	    data.y > box.lpos.yi && data.y < box.lpos.yl) {
		if (box.interactive) {
			line_hovered = parseInt(box.childBase + mousey);
		}
	}
	board_game();
	box.select(line_hovered - 1);
	box.setScroll(line_hovered + 3 - box.height);
	screen.render();
});

screen.key(['escape', 'q', 'C-c'], function(ch, key) {
	if (box == undefined || box.interactive)
		return process.exit(0);
	else {
		box.interactive = true;
		screen.render();
	}
});

diff.focus();

if (0)
ourBoard.connect('/dev/ttyUSB0').then(function(boardSerial) {
	ourBoard.on('ready',function() {
		ourBoard.streamStart();
		ourBoard.on('sample',function(sample) {
			if (channels === undefined) {
				for (var i = 0 ; i < 8 ; ++i)
					channels[i] = sample.channelData[i];
				for (var i = 0 ; i < 8 ; ++i)
					derivates[i] = sample.channelData[i];
			}
			if (sample.sampleNumber % 16 === 0) {
				for (var i = 0 ; i < 8 ; ++i)
					derivates[i] = sample.channelData[i] - channels[i];
				for (var i = 0 ; i < 8 ; ++i)
					channels[i] = sample.channelData[i];
				if ((derivates[0] > 0.00007        || derivates[0] < -0.00007    ) &&
				    (derivates[1] > 0.00007        || derivates[1] < -0.00007   )) {
					if (Math.abs(derivates[0] - derivates[1]) < 0.00001) {
						if (blink_type === 1 && (Date.now() - bascule) < 1024 &&
						    (Date.now() - both) > 1024) {
							var selected_difficulty = {content: diff.ritems[diff.selected]};
							var selected_lines = {content: get_lines.ritems[get_lines.selected]};
							if (diff.focused)
								getting_difficulty(selected_difficulty);
							else if (get_lines.focused)
								getting_lines(selected_lines);
							else if (box != undefined) {
								if (!lines[box.selected] && box.ritems[box.selected].match('you') === null) {
									logs.setContent('Error: this line is empty');
								} else if (!selecting_matches) {
									var selected_allumettes = {content: box.ritems[box.selected], parent: {selected: box.selected}};
									getting_allumettes(selected_allumettes);
									mousex = nb_lines + box.selected;
									selecting_matches = true;
									board_game();
								} else {
									rm_allumettes();
									selecting_matches = false;
								}
							}
							blink_type = 0;
							both = Date.now();
						}
						else
							bascule = Date.now();
						blink_type = 1;
					}
				}
				else if ((derivates[0] > 0.00006  || derivates[0] < -0.00006  ) &&
					 (derivates[1] < 0.00005  && derivates[1] > -0.00005)) {
					if (blink_type == 2) {
						if (diff.focused)
							diff.up(1);
						else if (get_lines.focused)
							get_lines.up(1);
						else if (selecting_matches) {
							if (mousex > 0)
								--mousex;
							board_game();
						}
						else if (box != undefined)
							box.up(1);
						blink_type = 0;
					} else
						bascule = Date.now();
					blink_type = 2;
				}
				else if ((derivates[1] > 0.000105 || derivates[1] < -0.000105 ) &&
					 (derivates[0] < 0.00005  && derivates[0] > -0.00005 )) {
					if (blink_type == 3) {
						if (diff.focused)
							diff.down(1);
						else if (get_lines.focused)
							get_lines.down(1);
						else if (selecting_matches) {
							if (mousex < (lines[line_hovered] - nb_lines + line_hovered))
								console.log(++mousex);
							board_game();
						}
						else if (box != undefined)
							box.down(1);
						blink_type = 0;
					} else
						bascule = Date.now();
					blink_type = 3;
				}
				else
					bascule = false;
				screen.render();
			}
		});
	});
}).catch(function(err) {
	console.log('ERROR\n' + err);
	screen.destroy();
});

function board_game()
{
	var cols = 2 * nb_lines,
	    rows = 0,
	    board = '';

	if (lost(0))
		return ;
	box.clearItems();
	while (++rows < (nb_lines + 1)) {
		cols = -1;
		if (line_hovered == rows)
			board += '{blue-fg}';
		while (++cols < (2 * nb_lines - 1)) {
			if ((cols + 1) == mousex && rows == line_hovered)
				board += '{red-fg}';
			board += (cols >= (nb_lines - rows) && (cols - nb_lines + rows < lines[rows - 1])) ? "|" : " ";
		}
		if (line_hovered == rows)
			board += '{red-fg}{/blue-fg}';
		box.add(board);
		board = '';
	}
	box.select(1 - line_hovered);
}

function pick_random_move(lines) {
	var nb = nb_lines;
	while (nb--)
		if (lines[nb]) {
			--lines[nb];
			break;
		}
	logs.setContent(logs.content + '\nAI removed 1 match from line ' + (1 + nb));
}

function pick_right_move(xor, lines) {
	var nb = nb_lines,
	    removed;
	while (nb--) {
		if ((lines[nb] ^ xor) < lines[nb]) {
			logs.setContent(logs.content + '\nAI removed ' + (removed = (lines[nb] - (lines[nb] ^= xor))) + ' match' + ((removed > 1) ? '(es) ' : ' ') + 'from line ' + (1 + nb));
			return (1);
		}
	}
	return (0);
}

function ai_turn() {
	var xor = base + parseInt(Math.random() * difficulty),
	    nb = nb_lines;

	while (nb--)
		xor ^= lines[nb];
	if ((nb = nb_lines) && !xor)
		return (pick_random_move(lines));
	if (xor && pick_right_move(xor, lines))
		return;
	nb = nb_lines;
	pick_random_move(lines);
}

function lost(player) {
	for (var i = 0 ; i < nb_lines ; ++i) {
		if (lines[i])
			return (0);
	}
	if (!player)
		return (1);
	box.clearItems();
	if (player == 1) {
		logs.setContent('You lost, too bad...');
		for (var i = 0 ; i < nb_lines ; ++i)
			box.add('you LOST');
	} else {
		logs.setContent('I lost... snif... but I\'ll get you next time!!');
		for (var i = 0 ; i < nb_lines ; ++i)
			box.add('you won');
	}
	return (1);
}

if (0)
rl.setPrompt('Line: ');
if (0)
rl.prompt();

if (0)
rl.on('line', (line) => {
	if (get_input[what_to_get](line.trim()) !== -1) {
		if (!what_to_get)
			what_to_get = 1;
		else {
			lines[selected_line] -= selected_matches;
			what_to_get = 0;
			console.log('Player remove ' + selected_matches + ' match' + ((selected_matches > 1) ? 'es ' : ' ') + 'from line ' + selected_line);
			board_game();
			if (lost(1))
				process.exit(0);
			console.log('AI\'s turn...');
			ai_turn();
			board_game();
			if (lost(2))
				process.exit(0);
		}
	} else if (what_to_get) {
		what_to_get = 0;
	}
	if (what_to_get)
		rl.setPrompt('Matches: ');
	else
		rl.setPrompt('Line: ');
	rl.prompt();
	console.log(rl);
}).on('close', () => {
	console.log('\nExiting...');
	process.exit(0);
});

screen.render();
