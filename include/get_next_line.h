/*
** get_next_line.h for get_next_line in /home/asphahyre/rendu/CPE_2015_getnextline
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Mon Jan 04 10:58:54 2016 Luka Boulagnon
** Last update Thu Feb 25 04:52:06 2016 Luka Boulagnon
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
# include <stdlib.h>
# include <unistd.h>

# ifndef READ_SIZE
#  define READ_SIZE (8)
# endif /* !READ_SIZE */

char		*get_next_line(const int);

#endif /* !GET_NEXT_LINE_H_ */
