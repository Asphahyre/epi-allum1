/*
** allum1.h for Allum1 in /home/asphahyre/rendu/CPE_2015_Allum1/include
** 
** Made by Luka Boulagnon
** Login   <boulag_l@epitech.net>
** 
** Started on  Sun Feb 21 14:34:17 2016 Luka Boulagnon
** Last update Wed Feb 24 19:11:04 2016 Luka Boulagnon
*/

#ifndef ALLUM1_H_
# define ALLUM1_H_
# include "get_next_line.h"
# include <unistd.h>
# include <stdio.h>

# ifndef NB_LINES
#  define NB_LINES 4
# endif /* !NB_LINES */

# ifndef DIFFICULTY
#  define DIFFICULTY 1
# endif /* !DIFFICULTY */

# define PLAYER 1
# define IA 2

# ifdef DEBUG
#  include <stdlib.h>
# endif /* DEBUG */

int		*init(int *);
int		lost(int);
int		main();
int		my_getnbr(char *);
int		pick_right_move(int, int *);
int		print_updated_board_game(int, int);
int		read_line_nb(char *, int *);
void		ia_turn();
void		my_bzero(char *, int);
void		my_putnbr(int);
void		pick_random_move(int *);
void		print_board_game_in_a_pretty_frame();
void		read_player_move_and_print_updated_board_game();

#endif /* !ALLUM1_H_ */
